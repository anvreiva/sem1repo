const

    exec = require('child_process').exec,
    nodemon = exec('nodemon'),
    server = exec('npm start'),
    logReact = data => console.log('[React] ' + data.toString()),
    logServer = data => console.log('[Server] ' + data.toString())
 
server.stdout.on('data', logReact)
server.stderr.on('data', logReact)
nodemon.stdout.on('data', logServer)
nodemon.stderr.on('data', logServer)