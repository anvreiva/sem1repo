import { NiceQuery } from './../src/core/shared'
import * as _ from 'lodash'
import { getYahooResults } from './getYahooResults'
import { getCacheGetter } from './getCacheGetter';

declare var Promise: any

const { getter, getSize } = getCacheGetter(getYahooResults, '[Results Cache] ')

const getterWrapper = (query, site) =>
    getter(query)
        .then(res =>
            res.map(res => ({ ...res, site }))
        )

const getSites = (defaults: string[], query: NiceQuery): string[] =>
    _(query.userQueries)
        .map(q => JSON.parse(q.text).sources)
        .flatten()
        .concat(defaults)
        .uniq()
        .value()

export const getAllYahooResults = async (query: NiceQuery): Promise<any[]> => {

    if (!query.text)
        return []

    let sites = getSites([
        'all internet', 'wikipedia.org', 'youtube.com', 'quora.com', 'reddit.com'
    ], query)

    const selectedSite = query.queryFilters.filter(filter => filter.property === 'site')[0].values[0]

    const bigYahooPromise = Promise.all(
        //simulate just so refiners generate themselves
        sites.map(site => site === selectedSite ?
            getterWrapper(
                query.text + (site === 'all internet' ? '' : (' site:' + site)),
                site
            ) :
            Promise.resolve([{ site }]))
    )

    const yahooResults = _(await bigYahooPromise).flatten().value()

    yahooResults.length || console.warn('no results from search engine')

    return yahooResults

}
