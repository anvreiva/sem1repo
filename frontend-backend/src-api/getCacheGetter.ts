import { NiceQuery } from "../src/core/shared";

function roughSizeOfObject(object) {

    var objectList = [];
    var stack = [object];
    var bytes = 0;

    while (stack.length) {
        var value = stack.pop();

        if (typeof value === 'boolean') {
            bytes += 4;
        }
        else if (typeof value === 'string') {
            bytes += value.length * 2;
        }
        else if (typeof value === 'number') {
            bytes += 8;
        }
        else if
            (
            typeof value === 'object'
            && objectList.indexOf(value) === -1
        ) {
            objectList.push(value);

            for (var i in value) {
                stack.push(value[i]);
            }
        }
    }
    return bytes;
}

//todo - what if it gets too big?
// use a cache so the search engine does not think we are a 'robot'
export const getCacheGetter = (getter: (query: string) => Promise<any[]>, logPrefix?: string) => {

    const cache = {}
    let size = 0
    const wrapper = async (query: string): Promise<any[]> => {

        if (cache[query]) {
            console.log(logPrefix + size + 'mb')
            return cache[query]
        }

        const results = await getter(query)
        size += roughSizeOfObject(results) / 1000000
        cache[query] = results
        return results
    }

    return {
        getter: wrapper,
        set: (key, val) => cache[key] = val,
        getSize: () => size
    }
}