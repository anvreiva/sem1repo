import { googleVerify } from "./signedInApi";
import { NiceQuery } from "../src/core/shared";

var fs = require('fs');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

var SCOPES = ['https://www.googleapis.com/auth/drive'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';

export function getDiskPathOfUser(userId) {
    return TOKEN_DIR + '/' + userId + '.json'
}

function getNewToken(oauth2Client, onAuthURLGenerated, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    })

    onAuthURLGenerated(authUrl).then(code => {
        oauth2Client.getToken(code, function (err, token) {
            if (err) {
                throw err
            }
            callback(token);
        });
    })
}

function storeToken(token, userId, resolve) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(getDiskPathOfUser(userId), JSON.stringify(token));
    resolve()
}

function getClientSecret(rej, callback) {
    fs.readFile(TOKEN_DIR + '/client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            rej('Error loading client secret file: ' + err)
            return
        }
        callback(JSON.parse(content))
    })
}

function getOAuth2Client(credentials) {
    var clientSecret = credentials.installed.client_secret;
    var clientId = credentials.installed.client_id;
    var redirectUrl = credentials.installed.redirect_uris[0];
    var auth = new googleAuth();
    return new auth.OAuth2(clientId, clientSecret, redirectUrl);
}

function authorize(credentials, userId, rej, callback) {

    const oauth2Client = getOAuth2Client(credentials)
    // Check if we have previously stored a token.
    fs.readFile(getDiskPathOfUser(userId), function (err, token) {
        if (err) { } else {
            oauth2Client.credentials = JSON.parse(token);
        }
        callback(oauth2Client)
    });
}

function listFiles(auth, res, rej, queryText) {
    var service = google.drive('v3');
    service.files.list({
        auth: auth,
        'fields': "files(name, webContentLink, mimeType)",
        pageSize: 10,
        'q': 'name contains "' + queryText + '" or fullText contains "' + queryText + '"',
    }, function (err, response) {
        if (err) {
            rej(err)
            return
        }
        res(response.files)

    });
}

// public api

export function getFiles(userId: string, queryText: string): Promise<any[]> {
    return new Promise((res, rej) => {
        getClientSecret(rej, cred => {
            authorize(cred, userId, rej, oAuthClient => {
                listFiles(oAuthClient, res, rej, queryText)
            });
        })
    })
}

export function register(userId: string, onAuthURLGenerated: (authURL) => Promise<string>): Promise<any> {

    return new Promise((res, rej) => {

        getClientSecret(rej, cred => {
            authorize(cred, userId, rej, client => {
                getNewToken(client, onAuthURLGenerated, token => {
                    storeToken(token, userId, res)
                })
            })
        })
    })
}

export function getGoogleDrivePromise(query: NiceQuery): Promise<any[]> {
    return new Promise((res, rej) => {
        if (!query.token) {
            res([])
            return
        }
        googleVerify(query.token)
            .then(({ email }) => getFiles(email, query.text))
            .then(files =>
                files.map(f =>
                    ({
                        title: f.name,
                        url: f.webContentLink,
                        site: 'drive',
                        text: f.mimeType
                    })
                )
            )
            .then(files => res(files))
    })
}
