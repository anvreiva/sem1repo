import * as fetch from 'node-fetch'
import * as  decode from 'decode-html'
import * as  cheerio from 'cheerio'

// ok user google after all even if it detects us as bot - make it not detect us by making just one query (not for each of the user's site) - also insert a fke result for each of the user's sites so that the refiners get generated

const getYahooURLFromQuery = (query) => {
    const terms = query.split(' ').filter(item => item)
    const link = 'https://google.ro/search?q=' + terms.map(encodeURIComponent).join('+')
    return link
}

export const getYahooResults = (query: string): Promise<any[]> => (console.log('made query for ' + query), fetch(getYahooURLFromQuery(query)))
    .then(res => res.text())
    .then(doc => {


        const $ = cheerio.load(doc)

        let results = []

        $('ol>.g').each((i, el) => {
            const a = $(el).find('h3>a')

            results.push({
                title: a.html(),
                niceUrl: $(el).find('cite').html(),
                url: 'https://google.ro/' + a.attr('href'),
                text: $(el).find('.st').html()
            })
        })

        results = results
            .filter(s => !!s.title && !!s.url)
            .map(s => {
                s.title = decode(s.title.replace(/<\/?[^>]+(>|$)/g, ""))
                s.url = decode(s.url.replace(/<\/?[^>]+(>|$)/g, ""))
                if (s.text) {
                    s.text = decode(s.text.replace(/<\/?[^>]+(>|$)/g, ""))
                }

                if (s.niceUrl) {
                    s.niceUrl = decode(s.niceUrl.replace(/<\/?[^>]+(>|$)/g, ""))
                }
                return s
            }) // remove html tags

        return results
    })  
