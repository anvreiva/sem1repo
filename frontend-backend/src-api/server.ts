import * as  bodyParser from 'body-parser'
import * as express from 'express'
import { getAllYahooResults } from './getAllYahooResults'
import { handleSignedInAction, googleVerify } from './signedInApi';
import { register, getGoogleDrivePromise } from './getGoogleDriveAPI';
import * as fs from 'fs'
import { getDiskPathOfUser } from './getGoogleDriveAPI';
const PORT = 3001,
    app = express()

app.use(require('cors')())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '10mb' }));

//site search

app.post('/_search', (req, res) =>
    getAllYahooResults(req.body)
        .then(r => res.json(r))
)

//singed in user actions

app.post('/action', (req, res) =>
    handleSignedInAction(req.body)
        .then(r => res.json(r))
)

//google drive endpoints

const driveUserNameToResolveMethod = {}

app.post('/drive-auth-request', (req, res) => {
    googleVerify(req.body.token).then((user: any) => {
        register(user.email, authURL => new Promise(resolve => {
            res.json({ authURL })
            driveUserNameToResolveMethod[user.email] = resolve
        }))
    })
})

app.post('/drive-auth-resolve', (req, res) => {

    driveUserNameToResolveMethod[req.body.userId](req.body.code)
    driveUserNameToResolveMethod[req.body.userId] = null
    res.json('done')
})

app.post('/_search-drive', (req, res) =>
    getGoogleDrivePromise(req.body)
        .then(r => res.json(r))
)

app.post('/user-has-drive', (req, res) =>
    googleVerify(req.body.token)
        .then((user: any) => {
            fs.exists(getDiskPathOfUser(user.email), exists => {
                res.json(exists)
            })
        })
)
app.post('/drive-revoke', (req, res) =>
    googleVerify(req.body.token)
        .then((user: any) => {
            fs.exists(getDiskPathOfUser(user.email), exists => {
                if (exists) {
                    fs.unlink(getDiskPathOfUser(user.email), err => {
                        res.json(err || 'ok')
                    })
                }

            })
        })
)

app.listen(PORT)
console.log('API server started on ' + PORT)