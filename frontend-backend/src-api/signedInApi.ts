import { SignedInAction, SignedInActionType, DbUser, DbQuery } from "../src/core/shared";
import { getCacheGetter } from "./getCacheGetter";

const fetch = require('node-fetch'),
    GoogleAuth = require('google-auth-library'),
    auth = new GoogleAuth,
    CLIENT_ID = '581166080618-bfr5jbp904v4g1egi8rssk4m20hppk7m.apps.googleusercontent.com',
    client = new auth.OAuth2(CLIENT_ID, '', '')

const genericDbCall = (url, method = 'get', data = null) => fetch('https://sem1-vasilescuandrei1079.c9users.io/' + url, {
    method: method, body: data ? JSON.stringify(data) : undefined,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
}).then(r => r.text())
    .then(r => {
        if (r == '"some error"') {
            throw Error('some error')
        }
        try {
            return JSON.parse(r)
        } catch (error) {
            return { msg: r }
        }
    })

export const googleVerify = (token: string) => new Promise((resolve, reject) => {
    client.verifyIdToken(
        token,
        CLIENT_ID,
        function (e, login) {
            if (e)
                reject(e)
            else {
                var payload = login.getPayload();
                resolve({
                    name: payload.name,
                    email: payload.email
                })
            }

        }
    );
})

const cachedGenericDbCall = getCacheGetter(genericDbCall, '[User queries cache] ')

export const handleSignedInAction = async (action: SignedInAction) => {

    const googleUser: any = await googleVerify(action.token)

    const users: DbUser[] = await genericDbCall('users')

    const dbUser = users.find(u => u.email === googleUser.email)

    if (!dbUser) {
        await genericDbCall('users', 'post', { ...googleUser, favQueryId: 0 })
    }

    const dbUserId = dbUser.id

    if (action.type === SignedInActionType.newQuery) {
        await genericDbCall('users/' + dbUserId + '/queries', 'post', { text: JSON.stringify({ sources: [], name: action.queryName }) })
    }

    if (action.type === SignedInActionType.deleteQuery) {
        const res = await genericDbCall('users/' + dbUserId + '/queries/' + action.queryId, 'delete')
    }

    if (action.type === SignedInActionType.addSourceToQuery) {
        const query: DbQuery = await genericDbCall('users/' + dbUserId + '/queries/' + action.queryId)
        const qObj = JSON.parse(query.text)
        const sources = qObj.sources

        if (sources.indexOf(action.source) !== -1)
            throw Error('duplicate source')

        sources.push(action.source)
        await genericDbCall('users/' + dbUserId + '/queries/' + action.queryId, 'put', {
            text: JSON.stringify(qObj)
        })
    }

    if (action.type === SignedInActionType.removeSourceFromQuery) {
        const query: DbQuery = await genericDbCall('users/' + dbUserId + '/queries/' + action.queryId)
        const qObj = JSON.parse(query.text)
        const sources = qObj.sources

        if (sources.indexOf(action.source) === -1)
            throw Error('source not found')

        sources.splice(sources.indexOf(action.source), 1)
        await genericDbCall('users/' + dbUserId + '/queries/' + action.queryId, 'put', {
            text: JSON.stringify(qObj)
        })
    }

    if (action.type !== SignedInActionType.getAllQueries) {
        //update the cache
        const newQueries = await genericDbCall('users/' + dbUserId + '/queries')
        cachedGenericDbCall.set('users/' + dbUserId + '/queries', newQueries)
    }

    const queries: DbQuery[] = await cachedGenericDbCall.getter('users/' + dbUserId + '/queries')

    return queries
}