import * as React from "react"
import {
  SearchkitManager, SearchkitProvider,
  SearchBox,
  Layout, TopBar, decodeObjString
} from 'searchkit'
import ProxyTransport from "../core/ProxyTransport";
import { store } from "../core/store";
import UserOptions from "./UserOptions";
import AppBody from "./AppBody";
import ToggleOptionsButton from "./ToggleOptionsButton";
import initializeState from "../core/initializeState";

declare global {
  interface Window {
    searchkit: SearchkitManager
  }
}

initializeState()

const searchkit = window.searchkit = new SearchkitManager('', {
  transport: new ProxyTransport()
})

export default class App extends React.Component<any, any> {

  constructor(props) {
    super(props)
    store.subscribe(() => this.forceUpdate()) //react will refresh only what's necessary when store state changes
  }

  render() {
    return (
      <SearchkitProvider searchkit={searchkit}>
        <Layout size="l">

          <TopBar>
            <ToggleOptionsButton />
            <SearchBox autofocus={true} searchOnChange={false} />
          </TopBar>
          
          <UserOptions />
          <AppBody />

        </Layout>
      </SearchkitProvider >
    );
  }
}
