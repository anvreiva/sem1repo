import { SearchkitComponent, LayoutBody, SideBar, LayoutResults, ViewSwitcherHits } from "searchkit";
import * as React from "react";
import TabsForEachUserQuery from "./TabsForEachUserQuery";
import { extend } from "lodash";
import { store, QueryState, GoogleSignInState } from "../core/store";
import axios from 'axios'

const HitsListItem = (props: any) => {
    const { bemBlocks, result } = props

    const source = result._source
    const isLocal = source.site === 'local'
    return (
        <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
            <div className={bemBlocks.item("details")}>
                <a href={source.url} onClick={(e) => {
                    if (isLocal) {
                        e.preventDefault()
                        axios.get('http://localhost:3003' + encodeURI(source.url))
                    }

                }}>
                    <h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{ __html: source.title }}>
                    </h2>
                </a>
                <h1 style={{ color: 'green', fontWeight: 600 }} className={bemBlocks.item("subtitle")}>{isLocal ? ('<local root>' + source.url) : source.niceUrl}
                </h1>
                <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{ __html: isLocal ? ('local ' + (source.text == 'dir' ? 'directory' : 'file')) : source.text }}>
                </div>
            </div>
        </div>
    )
}

export default class AppBody extends SearchkitComponent<any, any>{
    render() {
        const searchkit = window.searchkit
        const state = store.getState()
        const isSigendIn = !!state.signedInUserToken
        
        let shouldBeDisplayed = searchkit.results
            && searchkit.results.hits
            && searchkit.results.hits.hits
            // && searchkit.results.hits.hits.length
            && searchkit.query.index.queryString

        return (
            /* we still need TabsForEachUserQuery for the hidden refiner ( so the initial query has an aggregation ) so we do a display none not a conditional rendering */
            <div style={{ display: shouldBeDisplayed ? 'block' : 'none' }}>
                <LayoutBody>
                    <SideBar>
                        <TabsForEachUserQuery />
                    </SideBar>
                    <LayoutResults>
                        <ViewSwitcherHits
                            sourceFilter={['site', 'url', 'text', 'title']}
                            hitComponents={[
                                { key: "list", title: "List", itemComponent: HitsListItem, defaultOption: true }
                            ]}
                            scrollTo="body"
                        />
                    </LayoutResults>

                </LayoutBody>
            </div>
        )
    }

}