import * as React from "react";
import { ItemComponentProps, FastClick, block } from "searchkit";
import * as PropTypes from "prop-types";
import { store, dispatchImmediate } from "../core/store";
import { deleteQueryAction, deleteQuerySourceAction } from "../core/actions";

export class CustomCheckBoxItemComponent extends React.PureComponent<ItemComponentProps, any>{

    static contextTypes = {
        queryId: PropTypes.string
    }

    static defaultProps = {
        showCount: true,
        showCheckbox: true
    }

    render() {
        return itemRenderer(this.props, this.context.queryId)
    }
}

function itemRenderer(props: ItemComponentProps, queryId: string) {
    const {
      bemBlocks, onClick, active, disabled, style, itemKey,
        label, count, showCount, showCheckbox } = props
    const skBlock = bemBlocks.option
    const className = skBlock()
        .state({ active, disabled })
        .mix(bemBlocks.container("item"))

    const myWrapper = block('my-list-item-wrapper')

    const hasCount = showCount && (count != undefined) && (count != null)

    return (
        <div className={myWrapper} data-key={itemKey}>
            <div className={myWrapper.el('main')}>
                <FastClick handler={onClick}>
                    <div className={className} style={style} data-qa="option" >
                        {showCheckbox ? <input type="checkbox" data-qa="checkbox" checked={active} readOnly className={skBlock("checkbox").state({ active })} ></input> : undefined}
                        <div data-qa="label" className={skBlock("text")}>{label}</div>
                        {hasCount ? < div data-qa="count" className={skBlock("count")}>{count}</div> : undefined}
                    </div>
                </FastClick>
            </div>
            {label !== 'all internet' &&
                (<div onClick={() => dispatchImmediate(deleteQuerySourceAction(Number(queryId), label))}
                    className={myWrapper.el('cross')}>
                    x
                </div>)
            }
        </div>
    )
}