import { MenuFilter } from "searchkit";
import { isUndefined } from "util";
import { defaults, concat } from "lodash";
import * as PropTypes from "prop-types";

export class CustomMenuFilter extends MenuFilter {

    static childContextTypes = {
        queryId: PropTypes.string
    }
    getChildContext() {
        return {
            queryId: this.props.queryId
        }
    }
    getSelectedItems() {
        let selectedValue = this.accessor.state.getValue()[0]

        if (!isUndefined(selectedValue))
            return selectedValue

        // no items selected
        const items = this.getItems()
        const allInternet = items[1]
        const theHiddenAllValue = items[0]

        // no items selected but we have allInternet results
        if (!isUndefined(allInternet)) {

            // if the user has clicked this, the state in the url is lost, get it back
            setImmediate(() => {
                let selectedValue = this.accessor.state.getValue()[0]
                if (!selectedValue)
                    this.toggleFilter(allInternet.key)
            })
            return allInternet.key
        }

        // there are no all internet results because the user has not typed anything yet
        return theHiddenAllValue.key

    }
    getItems() {
        const all = {
            key: '$all',
            label: 'All',
            doc_count: this.accessor.getDocCount()
        }

        const items = super.getItems()

        if (!this.props.includeSources)
            return items

        return concat([all],
            items.filter(item => this.props.includeSources.indexOf(item.key) !== -1)
        )
    }
}