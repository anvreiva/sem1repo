import { Panel, block } from "searchkit";
import * as React from "react";
import { store, dispatchImmediate } from "../core/store";
import { deleteQueryAction, newQueryAction, addQueryAction, addSourceToQueryAction } from "../core/actions";
import * as PropTypes from "prop-types";
import { DynamicInput } from "./DynamicInput";
// override the Panel to include our + and x buttons
export class CustomPanel extends Panel {

    dynamicInput: DynamicInput;

    static contextTypes = {
        queryId: PropTypes.string
    }

    render() {

        const { title, mod, className, disabled, children, collapsable } = this.props
        const collapsed = collapsable && this.state.collapsed
        var containerBlock = block(mod)
            .state({ disabled })

        var titleDiv

        const myPanelHeaderWrapper = block('my-panel-header-wrapper')

        if (collapsable) {
            titleDiv = (
                <div className={containerBlock.el("header").state({ collapsable, collapsed })} onClick={this.toggleCollapsed.bind(this)}>
                    {title}
                </div>
            )
        } else {

            titleDiv = (
                <div className={myPanelHeaderWrapper}>
                    <div className={containerBlock.el("header")}>
                        {title}
                    </div>
                    <div onClick={() => this.dynamicInput.show()}
                        className={myPanelHeaderWrapper.el('add')}>
                        +
                    </div>
                    <div onClick={() => dispatchImmediate(deleteQueryAction(Number(this.context.queryId)))}
                        className={myPanelHeaderWrapper.el('cross')}>
                        x
                    </div>
                </div >
            )
        }

        return (
            <div className={containerBlock.mix(className)}>
                {titleDiv}
                <div className={containerBlock.el("content").state({ collapsed })}>
                    {children}
                </div>
                <DynamicInput
                    ref={di => this.dynamicInput = di}
                    onSubmit={val => dispatchImmediate(addSourceToQueryAction(Number(this.context.queryId), val))}
                />
            </div>
        );

    }
}