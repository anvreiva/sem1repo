import { SearchkitComponent } from "searchkit";
import * as React from "react";
import { store, findSource, dispatchImmediate } from "../core/store";
import { logInAction, logOutAction } from "../core/actions";

export default class DriveOptions extends SearchkitComponent<any, any>{
    render() {

        const state = store.getState()
        const driveSource = findSource(store.getState(), 'drive')
        const isSignedIn = !!state.signedInUserToken
        const isDriveAuthenticated = driveSource ? driveSource.isAuthenticated : false

        if (driveSource && isSignedIn) {
            return (
                <div>
                    <button onClick={() => dispatchImmediate(
                        !isDriveAuthenticated ?
                            logInAction('drive') :
                            logOutAction('drive')
                    )}>
                        {isDriveAuthenticated ? 'Sign out google drive' : 'Sign in google drive'}
                    </button>
                </div>
            )
        }

        return null
    }
}