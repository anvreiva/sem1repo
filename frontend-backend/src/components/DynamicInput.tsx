import * as React from "react";

export interface DynamicInputProps {
    onSubmit: (enteredValue: string) => void
}

export class DynamicInput extends React.Component<DynamicInputProps, any>{

    textInput: HTMLInputElement

    //called by parent whenever
    show() {

        this.textInput.style.display = 'block'
        this.textInput.focus()
        this.textInput.onblur = () =>
            (
                this.textInput.style.display = 'none',
                this.textInput.value = ''
            )
        this.textInput.onkeypress = e => {

            if (e.keyCode !== 13)
                return true

            this.props.onSubmit(this.textInput.value)
            this.textInput.blur()
            this.textInput.value = ''
        }
    }

    render() {

        return (
            <input style={{ display: 'none' }} ref={input => this.textInput = input} />
        )
    }
}