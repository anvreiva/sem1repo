import { SearchkitComponent, Toggle, ItemList, CheckboxItemList, CheckboxItemComponent, block } from "searchkit";
import * as React from "react";
import { CustomMenuFilter } from "./CustomMenuFilter";
import { store, dispatchImmediate } from "../core/store";
import { CustomCheckBoxItemComponent } from "./CustomCheckBoxItemComponent";
import { CustomPanel } from "./CustomPanel";
import { FacetFilterProps } from "searchkit/lib/components/search/filters/facet-filter/FacetFilterProps";
import { DynamicInput } from "./DynamicInput";
import { addQueryAction } from "../core/actions";

declare module "searchkit/lib/components/search/filters/facet-filter/FacetFilterProps" {
    interface FacetFilterProps {
        queryId?: string,
        includeSources?: string[]
    }
}
export default class TabsForEachUserQuery extends SearchkitComponent<any, any>{
    dynamicInput: DynamicInput;
    render() {
        const state = store.getState()
        const queries = state.userQueries
        const noUserQueries = !queries.length

        const tabsBlock = block('tabs-for-each-user-query-header')

        return (
            <div className={tabsBlock}>
                <div style={{ visibility: (!state.signedInUserToken) ? 'hidden' : 'visible' }}
                    onClick={() => this.dynamicInput.show()}
                    className={tabsBlock.el('add-query')}>
                    +
                </div>
                <div className="full-width" />
                <DynamicInput
                    onSubmit={qName => dispatchImmediate(addQueryAction(qName))}
                    ref={di => this.dynamicInput = di}
                />
                <div className="full-width">
                    {/* if singed out, this CustomMenuFilter has some sample sites
                        if signed in, it has the list of sources (all internet, drive etc)
                     */}
                    <CustomMenuFilter 
                        key={'-1'}
                        field={'site'}
                        title={''}
                        id={'site'}
                        includeSources={noUserQueries ? undefined /*include all*/ : ['drive', 'all internet','local']}
                        listComponent={CheckboxItemList}
                        itemComponent={CheckboxItemComponent}
                        showCount={false} />
                    {queries.map(q => (
                        <CustomMenuFilter
                            includeSources={JSON.parse(q.text).sources}
                            key={q.id.toString()}
                            queryId={q.id.toString()}
                            field={'site'}
                            title={JSON.parse(q.text).name}
                            id={'site'}
                            containerComponent={CustomPanel}
                            listComponent={CheckboxItemList}
                            itemComponent={CustomCheckBoxItemComponent}
                            showCount={false} />
                    ))}
                </div>
            </div>
        )
    }
}