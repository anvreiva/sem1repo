import { SearchkitComponent } from "searchkit";
import * as React from "react";
import { dispatchImmediate, store } from "../core/store";
import { toggleUserOptionsVisibility } from "../core/actions";

export default class ToggleOptionsButton extends SearchkitComponent<any, any> {
    render() {
        return (
            <div
                className="my-logo"
                onClick={() => dispatchImmediate(toggleUserOptionsVisibility())}>
                Search Fiesta 3
            </div>
        )
    }
}