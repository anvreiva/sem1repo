import { SearchkitComponent, LayoutBody } from "searchkit";
import * as React from "react";
import { GoogleAuthenticator } from "./GoogleAuthenticator";
import { store, dispatchImmediate, findSource } from "../core/store";
import { logInAction, logOutAction } from "../core/actions";
import DriveOptions from "./DriveOptions";

export default class UserOptions extends SearchkitComponent<any, any>{
    render() {
        return (
            <div style={{ display: store.getState().optionsVisible ? 'block' : 'none' }} className={'sk-layout__tabs'} >
                <LayoutBody>
                    <div>
                        <GoogleAuthenticator />
                        <DriveOptions />
                    </div>
                </LayoutBody>
            </div>
        )
    }
}

