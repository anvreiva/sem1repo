import { ESTransport } from "searchkit"
import { translateQuery } from './translateQuery'
import { dispatchImmediate, store } from "./store";
import { getResultsAction } from "./actions";

export default class ProxyTransport extends ESTransport {
  search(query: Object) {
    return new Promise(resolve => {
      const translatedQuery = translateQuery(query, store.getState())
      dispatchImmediate(getResultsAction(translatedQuery, resolve))
    })
  }
}