import { NiceQuery } from "./shared";
import { preProcessRawResults } from "./preProcessRawResults";
import { translateResults, NiceResults } from "./translateResults";
import { State, QueryState, store, Source, dispatchImmediate, findSource, GoogleSignInState } from "./store";
import * as _ from "lodash";
import { setImmediate } from "timers";
import axios from 'axios'
import { SignedInAction, SignedInActionType, DbQuery } from "./shared";

export function getResultsAction(query: NiceQuery, onCompleted) {
    function getResultsReducer(state: State): State {

        function onAllResultsReceived(resultsfromAllSources: any[]) {
            const niceResults = preProcessRawResults(resultsfromAllSources, query);
            dispatchImmediate(receivedResultsAction(niceResults));
            setImmediate(() => onCompleted(translateResults(niceResults)));
        }

        if (!query.text) {
            onAllResultsReceived([])
        } else {

            Promise.all(state.sources.filter(s => s.isAuthenticated)
                .map(s => s.getResults(query)))
                .then(allResults =>
                    onAllResultsReceived(
                        _(allResults)
                            .map(r => r.data)
                            .flatten()
                            .value()
                    )
                )
        }

        return {
            ...state,
            querystate: QueryState.pending
        }
    }

    return {
        reducer: getResultsReducer,
        type: getResultsAction.name,
        log: query
    }
}

export function receivedResultsAction(results: NiceResults) {
    return {
        reducer: (state): State => ({
            ...state,
            results: results,
            querystate: QueryState.succeeded
        }),
        type: receivedResultsAction.name
    }
}

export function addSourceAction(
    name: string,
    getter: (query: NiceQuery) => Promise<any[]>,
    isAuthenticated: boolean = true,
    authenticate: () => void = () => { },
    unAuthenticate: () => void = () => { }) {

    return {
        reducer: (state: State): State => {
            state.sources.push({
                name: name,
                getResults: getter,
                isAuthenticated,
                authenticate,
                unAuthenticate
            })
            return state
        },
        type: addSourceAction.name
    }
}

export function logInAction(source: string) {
    return {
        type: logInAction.name,
        reducer: (state: State): State => {
            findSource(state, source).authenticate()
            return state
        }
    }
}

export function logOutAction(source: string) {
    return {
        type: logOutAction.name,
        reducer: (state: State): State => {
            findSource(state, source).unAuthenticate()
            return state
        }
    }
}

export function newQueryAction() {
    return {
        type: newQueryAction.name,
        reducer: (state: State) => {
            window.searchkit.performSearch()
            return state
        }
    }
}

export function sourceLoggedInAction(source: string) {

    return {
        type: sourceLoggedInAction.name,
        reducer: (state: State): State => {
            findSource(state, source).isAuthenticated = true
            dispatchImmediate(newQueryAction())
            return state
        }
    }
}

export function sourceLoggedOutAction(source: string) {

    return {
        type: sourceLoggedOutAction.name,
        reducer: (state: State): State => {
            findSource(state, source).isAuthenticated = false
            dispatchImmediate(newQueryAction())
            return state
        }
    }
}

export function signedInAction(email: string, authToken: string) {

    return {
        type: signedInAction.name,
        reducer: (state: State): State => {
            state.signedInEmail = email
            localStorage.setItem('lastUserEmail', email)
            state.signedInUserToken = authToken
            dispatchImmediate(getQueriesAction())

            // if user singed in with another account, remove the old drive source

            _(state.sources).remove(s => s.name == 'drive')

            // has drive ?

            axios.post('http://localhost:3001/user-has-drive', { token: authToken }).then(({ data }: any) => {
                dispatchImmediate(
                    addSourceAction(
                        'drive',
                        query => <Promise<any>>axios.post('http://localhost:3001/_search-drive', query),
                        !!data,
                        () => axios.post('http://localhost:3001/drive-auth-request', { token: authToken })
                            .then((resp: any) => location = resp.data.authURL),
                        () => axios.post('http://localhost:3001/drive-revoke', { token: authToken })
                            .then((resp: any) => {
                                resp.data == 'ok' && dispatchImmediate(sourceLoggedOutAction('drive'))
                            })
                    )
                )
            })


            return state
        }
    }
}

export function signedOutAction() {

    return {
        type: signedOutAction.name,
        reducer: (state: State): State => {
            state.signedInEmail = ''
            state.signedInUserToken = ''
            state.userQueries = []
            dispatchImmediate(newQueryAction())
            return state
        }
    }
}

const genericSignedInAction = (action: SignedInAction) =>
    axios.post('http://localhost:3001/action', action)
        .then(res =>
            dispatchImmediate(
                queriesReceivedAction(res.data)
            )
        )

const queriesReceivedAction = (queries: DbQuery[]) => ({
    type: queriesReceivedAction.name,
    reducer: (state: State): State => {

        dispatchImmediate(newQueryAction())
        state.userQueries = queries
        return state
    }
})

const getQueriesAction = () => ({
    type: getQueriesAction.name,
    reducer: (state: State): State => (
        genericSignedInAction({
            token: state.signedInUserToken,
            type: SignedInActionType.getAllQueries
        }),
        state
    )
})

export const addQueryAction = (name: string) => ({
    type: addQueryAction.name,
    reducer: (state: State): State => (
        genericSignedInAction({
            token: state.signedInUserToken,
            type: SignedInActionType.newQuery,
            queryName: name
        }),
        state
    )
})

export const deleteQueryAction = (id: number) => ({
    type: deleteQueryAction.name,
    reducer: (state: State): State => (
        genericSignedInAction({
            token: state.signedInUserToken,
            type: SignedInActionType.deleteQuery,
            queryId: id
        }),
        state
    )
})

export const addSourceToQueryAction = (qid: number, source: string) => ({
    type: addSourceToQueryAction.name,
    reducer: (state: State): State => (
        genericSignedInAction({
            token: state.signedInUserToken,
            type: SignedInActionType.addSourceToQuery,
            source,
            queryId: qid
        }),
        state
    )
})

export const deleteQuerySourceAction = (qid: number, source: string) => ({
    type: deleteQuerySourceAction.name,
    reducer: (state: State): State => (
        genericSignedInAction({
            token: state.signedInUserToken,
            type: SignedInActionType.removeSourceFromQuery,
            queryId: qid,
            source
        }),
        state
    )
})

export const toggleUserOptionsVisibility = () => ({
    type: toggleUserOptionsVisibility.name,
    reducer: (state: State): State => ({
        ...state,
        optionsVisible: !state.optionsVisible
    })
})

export const resolveGoogleDriveRegistration = (code) => ({
    type: resolveGoogleDriveRegistration.name,
    reducer: (state: State): State => {
        axios.post('http://localhost:3001/drive-auth-resolve', {
            userId: localStorage.getItem('lastUserEmail'),
            code
        })
        return state
    }
})