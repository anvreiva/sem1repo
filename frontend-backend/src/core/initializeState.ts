import { SearchkitManager, SearchRequest } from "searchkit";
import { isEqual } from "lodash";
import * as qs from 'qs'
import { dispatchImmediate, store } from "./store";
import { resolveGoogleDriveRegistration } from "./actions";

export default function initializeState() {
    let allInternet = 'site[0]=all%20internet'
    if (location.toString().indexOf('site[0]=') === -1) {

        if (location.toString().indexOf('?') === -1) {
            allInternet = '?' + allInternet
        } else {
            allInternet = '&' + allInternet
        }

        const driveRedirectCode = qs.parse(location.search)['?code']

        if (driveRedirectCode) {
            store.dispatch(resolveGoogleDriveRegistration(driveRedirectCode))
        }

        const newUrl = (location + allInternet)
            .replace(/\+/g, "%20")
            .replace('#', '')

        window.location.replace(newUrl)
        throw 1 // stop everything from executing and just reload the page
    }

    SearchkitManager.prototype._search = function () {
        this.state = this.accessors.getState()
        let query = this.buildQuery()
        if (!this.shouldPerformSearch(query)) {
            return Promise.resolve(this.getResultsAndState())
        }
        //never do this cause this query does not take into account the whole query state
        // if (this.results && this.query && isEqual(query.getJSON(), this.query.getJSON())) {
        //     return Promise.resolve(this.getResultsAndState())
        // }
        this.query = query
        this.loading = true
        this.emitter.trigger()
        let queryObject = this.queryProcessor(this.query.getJSON())
        this.currentSearchRequest && this.currentSearchRequest.deactivate()
        this.currentSearchRequest = new SearchRequest(
            this.transport, queryObject, this)
        return this.currentSearchRequest.run()
            .then(() => {
                return this.getResultsAndState()
            })
    }
}