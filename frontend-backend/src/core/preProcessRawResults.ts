import { NiceResults, AggResult, Bucket } from "./translateResults";
import { NiceQuery, NiceAggregation, NiceFilter } from "./shared";
import { unlink } from "fs";
import * as _ from "lodash";

//constructs aggregations based on raw hits
export const preProcessRawResults = (rawHits: any[], originalQuery: NiceQuery): NiceResults => {

    const respectsFilter = (hit, filter: NiceFilter) => {
        const hitValue = hit[filter.property]

        // if property is not present in raw results but filter requires it, consider the filter to be respected
        if (hitValue === undefined)
            return true

        // the implicit operator between value is or
        return _(filter.values).some(filterValue => hitValue == filterValue)

    }
    const respectsFilters = (hit, filters: NiceFilter[]) => {
        return _(filters).every(filter => respectsFilter(hit, filter))
    }

    // todo return results based on the original query
    return {
        aggregations: originalQuery.aggregations.map((agg: NiceAggregation): AggResult => {

            const filteredHits = rawHits.filter(hit => respectsFilters(hit, agg.aggFilters))

            const valueToCountMap = filteredHits.reduce((valueToCountMap, hit) => {

                const value = hit[agg.property]

                if (valueToCountMap[value] === undefined)
                    valueToCountMap[value] = 0
                    
                valueToCountMap[value]++

                return valueToCountMap
            }, {})

            const buckets = Object.keys(valueToCountMap).map((value): Bucket => {
                return {
                    value: value,
                    count: valueToCountMap[value]
                }
            })

            return {
                buckets: buckets,
                count: buckets.length,
                id: agg.id,
                property: agg.property
            }
        }),

        hits: rawHits.filter(hit => respectsFilters(hit, originalQuery.queryFilters))
    }
}