export enum SignedInActionType {
    newQuery,
    deleteQuery,
    addSourceToQuery,
    removeSourceFromQuery,
    getAllQueries
}

export interface SignedInAction {
    token: string
    type: SignedInActionType,
    queryId?: number,
    source?: string
    queryName?: string
}

export interface DbUser {
    id: string,
    name: string,
    email: string,
    favQueryId: number
}

export interface DbQuery {
    id: number
    text: string,
    user_id: string
}


export interface NiceFilter {

    property: string
    values: Array<string>
}

export interface NiceSort {

    property: string
    type: string
}

export interface NiceAggregation {

    aggFilters: Array<NiceFilter>
    id: string
    property: string
    size: number
}

export interface NiceQuery {

    text: string
    queryFilters: Array<NiceFilter>
    aggregations: Array<NiceAggregation>
    properties: Array<string>
    resultSize: number
    sort: NiceSort,
    userQueries: DbQuery[],
    token?: string
}