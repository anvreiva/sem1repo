import { Reducer } from "redux";
import { NiceQuery } from "./shared";
import { NiceResults } from "./translateResults";
import * as redux from 'redux'
import { preProcessRawResults } from "./preProcessRawResults";
import axios from 'axios'
import { addSourceAction } from "./actions";
import { setImmediate } from "timers";
import { DbQuery } from "./shared";

export interface Source {
    name: string,
    getResults: (query: NiceQuery) => Promise<any>
    isAuthenticated: boolean,
    authenticate: () => void,
    unAuthenticate: () => void
}

export interface State {

    refreshQuery: () => void
    results: NiceResults,
    sources: Source[],
    querystate: QueryState,
    signedInEmail: string,
    signedInUserToken: string
    userQueries: DbQuery[],
    optionsVisible: boolean
}

export function findSource(state: State, name: string): Source {
    return state.sources.find(s => s.name === name)
}

export enum QueryState {
    pending,
    succeeded // even if no results
}

export enum GoogleSignInState {
    signedOut,
    SignedIn
}

const log = obj => console.log(JSON.parse(JSON.stringify(obj)))

export const store = redux.createStore((state: State, action: { type, log, reducer: (state: State) => State }): State => {

    state = state || {
        results: {
            aggregations: [],
            hits: []
        },
        sources: [],
        querystate: QueryState.succeeded,
        refreshQuery: () => { console.error('sk manager not registered') },
        userQueries: [],
        optionsVisible: false,
        signedInUserToken: '',
        signedInEmail: ''

    }

    const newState = action.reducer ? // not initial state by redux
        action.reducer(state)
        : state

    log({ [action.type]: action.log || 'no additional log', newState })

    return newState
})

//sometimes reducers needs to dispatch actions - avoid inifinte reccursion
export const dispatchImmediate = what =>
    setImmediate(() =>
        store.dispatch(what))

dispatchImmediate(
    addSourceAction(
        'yahoo',
        query => <Promise<any>>axios.post('http://localhost:3001/_search', query)
    )
)

axios.post('http://localhost:3003/search/', { q: 'a' }).then(_ =>
    dispatchImmediate(
        addSourceAction('local files',
            query => <Promise<any>>axios.post('http://localhost:3003/search/', { q: query.text }).then(resp =>
                ({
                    data: resp.data.map(({ Path, Name, Type }) =>
                        ({
                            url: Path,
                            title: Name,
                            text: Type,
                            site: 'local'
                        })
                    )
                })
            )
        )
    )
)