import * as _ from 'lodash'
import { State } from './store';
import { DbQuery, NiceFilter, NiceQuery, NiceAggregation } from './shared';

// only supports 'normal refiners' that are on 'OR' mode
// no prefixQueryFields, no suggestions, no highlight => the sk components that request this should not be included
export const translateQuery = (query, state: State): NiceQuery => {

    query.aggs = query.aggs || {}
    const findFiltersIn = (obj): NiceFilter[] => {

        //a recursive function that will search for all nested inner objects that have a property prop
        const findInnerObjectsWithProp = (obj, prop: string) => {

            const res = []

            if (!obj)
                return res

            if (obj[prop]) {
                res.push(obj)
            }

            Object.keys(obj).filter(k => typeof obj[k] == 'object').forEach(k => {
                res.push.apply(res, findInnerObjectsWithProp(obj[k], prop))
            })

            return res
        }

        const rawInnerObjects = findInnerObjectsWithProp(obj, 'term')

        const termObjects = rawInnerObjects.map(t => ({
            property: Object.keys(t.term)[0],
            value: _.values(t.term)[0]
        }))

        return _(termObjects).reduce((acc, current) => {

            if (!acc.filter(item => item.property === current.property).length) {
                acc.push({
                    property: current.property,
                    values: []
                })
            }

            acc.filter(item => item.property === current.property)[0].values.push(current.value)

            return acc

        }, [])

    }

    const getQueryFilters = (): Array<NiceFilter> => findFiltersIn(query.post_filter)

    const getAggregations = (): Array<NiceAggregation> => Object.keys(query.aggs).map(aggName => {

        const agg = query.aggs[aggName]

        const firstInnerAggName = Object.keys(agg.aggs)
            .filter(k => k.slice(-6) !== '_count')[0]

        return {
            aggFilters: findFiltersIn(agg),
            id: aggName,
            property: agg.aggs[firstInnerAggName].terms.field,
            size: agg.aggs[firstInnerAggName].terms.size
        }
    })

    return {
        queryFilters: getQueryFilters(),
        aggregations: getAggregations(),
        properties: <string[]>query._source,
        resultSize: <number>query.size,
        sort: query.sort ? {
            property: <string>Object.keys(query.sort[0])[0],
            type: <string>_.values(query.sort[0])[0]
        } : null,
        text: query.query ? query.query.simple_query_string.query : '',
        userQueries: state.userQueries,
        token:state.signedInUserToken
    }
}