export interface Bucket {
    value: string
    count: number
}
export interface AggResult {
    id: string
    property: string
    count: number // items which respect the filter
    buckets: Bucket[]

}
export interface NiceResults {
    hits: any[]
    aggregations: AggResult[]
}

export const translateResults = (results: NiceResults) => {

    return {
        took: 100,
        timed_out: false,
        "_shards": {
            "total": 4,
            "successful": 4,
            "failed": 0
        },
        hits: {
            total: results.hits.length,
            "max_score": results.hits.length,
            hits: results.hits.map(hit => ({
                "_index": "bla",
                "_type": "bla",
                "_id": Math.random().toString(),
                "_score": results.hits.length + 1,
                "_source": hit
            })),
        },
        aggregations: results.aggregations.reduce((acc, agg) => {

            const aggItem = acc[agg.id] = {
                "doc_count": agg.count
            }
            aggItem[agg.property + '_count'] = {
                "value": agg.buckets.length
            }
            aggItem[agg.property] = {
                "doc_count_error_upper_bound": 0,
                "sum_other_doc_count": 0,
                buckets: agg.buckets.map(b => ({
                    key: b.value,
                    'doc_count': b.count
                }))
            }

            return acc
        }, {})

    }
}