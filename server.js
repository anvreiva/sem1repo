const app = require('express')(),
  schema = require('./src/defineSchema')()

app.use(require('body-parser').json())

app.get('/create', (req, res, next) => next(

  schema.sequelize.sync({
    force: true
  })
  .then(()=>'done')

))


require('./src/createUserEndpoints')(app, schema)

require('./src/createQueriesEndpoints')(app, schema)

require('./src/useInfoMiddleWare')(app)


app.listen(8080)