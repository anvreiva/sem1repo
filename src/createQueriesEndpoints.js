module.exports = function (app, schema) {

    const User = schema.User,
        Query = schema.Query

    app.get('/users/:uid/queries', (req, res, next) => next(

        User.findById(req.params.uid)
        .then(user => user.getQueries())

    ))

    app.get('/users/:uid/queries/:qid', (req, res, next) => next(

        Query.findById(req.params.qid) // todo check
    ))

    app.post('/users/:uid/queries', (req, res, next) => next(

        User.findById(req.params.uid)
        .then(user => {

            let query = req.body
            query.user_id = user.id
            return Query.create(query)

        })
        .then(() => 'done')

    ))

    app.put('/users/:uid/queries/:qid', (req, res, next) => next(

        Query.findById(req.params.qid)
        .then(query => query.update(req.body))
        .then(() => 'done')

    ))

    app.delete('/users/:uid/queries/:qid', (req, res, next) => next(

        Query.findById(req.params.qid)
        .then(query => query.destroy())
        .then(() => 'done')

    ))
}