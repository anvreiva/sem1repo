module.exports = function (app, schema) {

    const User = schema.User,
        Query = schema.Query

    app.get('/users', (req, res, next) => next(

        User.findAll()

    ))

    app.get('/users/:id', (req, res, next) => next(

        User.findById(req.params.id, {
            include: [Query]
        })

    ))

    app.post('/users', (req, res, next) => next(

        User.create(req.body)
        .then(() => 'done')

    ))

    app.put('/users/:id', (req, res, next) => next(

        User.findById(req.params.id)
        .then(user => user.update(req.body))
        .then(() => 'done')

    ))

    app.delete('/users/:id', (req, res, next) => next(

        User.findById(req.params.id)
        .then((user) => user.destroy())
        .then(() => 'done')

    ))
}