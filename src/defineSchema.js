const Sequelize = require('sequelize')

module.exports = function () {

    const sequelize = new Sequelize('test', 'root', '', {
        dialect: 'mysql',
        define: {
            timestamps: false
        }
    })

    let User = sequelize.define('user', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                len: [3, 40]
            }
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                isEmail: true
            }
        }
    }, {
        underscored: true
    })

    let Query = sequelize.define('query', {
        text: {
            type: Sequelize.TEXT,
            allowNull: false
        }
    }, {
        underscored: true
    })

    User.hasMany(Query)

    return {
        sequelize: sequelize,
        User: User,
        Query: Query
    }
}