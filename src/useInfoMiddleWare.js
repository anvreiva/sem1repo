module.exports = function (app) {

    app.use((promise, req, res, next) => {

        // always use same error code for security
        promise.then(
            value => {
                let status = 200
                if (!value) {
                    value = 'some error'
                    status = 500
                }

                res.status(status).json(value)
                console.log(value)
            },
            err => {res.status(500).json('some error'); console.log(err);}
        )
    })
    
}